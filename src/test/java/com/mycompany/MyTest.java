/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.mavenproject3.example1.MyLamda;
import com.mycompany.mavenproject3.example1.Greeter;
import com.mycompany.mavenproject3.example1.Greeting;
import com.mycompany.mavenproject3.example1.HellowGreetingImp;
import com.mycompany.mavenproject3.exersice.Person;
import com.mycompany.mavenproject3.exersice.SortByName;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import org.junit.Test;

/**
 *
 * @author ahmed
 */
interface Condition {

    boolean checkCondition(Person p);
}

public class MyTest {

    @Test
    public void givenList_whenFilter_thenWillFilterUsingJava7AnonymousClass() {
        List<Person> people = Arrays.asList(
                new Person("Name1", "Name2", 12),
                new Person("Name3", "Name4", 13),
                new Person("Name5", "Name6", 14),
                new Person("Name7", "_Name8", 15)
        );
        Collections.sort(people, new SortByName());
        printAll(people);
        System.out.println("=====================");
        printLastNameBeginWithN(people, new Condition() {//anonymous class
            @Override
            public boolean checkCondition(Person p) {
                return p.getLastName().startsWith("N");
            }

        });
        System.out.println("=====================");
    }

    @Test
    public void givenList_whenFilter_thenWillFilterUsingJava8LamdaExpressions() {
        List<Person> people = Arrays.asList(
                new Person("Name1", "Name2", 12),
                new Person("Name3", "Name4", 11),
                new Person("Name5", "Name6", 13),
                new Person("Name7", "MyName8", 10)
        );
        Collections.sort(people, (Person p1, Person p2)
                -> p1.getAge() - p2.getAge());
        printAll(people);
        System.out.println("=====================");
        //We don't specify the type of the parameter because he defined 
        //in Condition class
        printLastNameBeginWithN(people, p -> p.getLastName().startsWith("N"));
        System.out.println("=====================");
        printLastNameBeginWithM(people,p -> p.getLastName().startsWith("M"));

    }

    private void printAll(List<Person> people) {
        for (Person person : people) {
            System.out.println(person);
        }
    }

    private void printLastNameBeginWithN(List<Person> people, Condition condition) {
        for (Person person : people) {
            if (condition.checkCondition(person)) {
                System.out.println(person);
            }
        }
    }

    private void printLastNameBeginWithM(List<Person> people, Predicate<Person> predicate) {
        for (Person person : people) {
            if (predicate.test(person)) {
                System.out.println(person);
            }
        }
    }
}
